#!/bin/env python3

from quart import Quart, websocket, abort
import os
import vbuild
import re

app = Quart(__name__)

print (
"""
  --=--
 - qvp -
  --=--
""")

# Mimic Nuxt API

template = """
<!doctype html>
<html>
<head>
<title>qvp</title>
<link rel="icon" type="image/png" href="icon.png" />
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=5, minimal-ui" />
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<!-- head -->
<style>
/* style */
</style>
</head>
<body>
<div id="qvp_app">
<!-- layout -->
<!-- html -->
</div>
<script>
/* script */
new Vue({el:"#qvp_app"})
</script>
</body>
</html>
"""

render_paths = ['components/*.vue']


@app.route('/')
@app.route('/<string:page>')
async def serve(page='index'):
    page_file = os.path.join('pages', page + '.vue')
    page_name = "pages-" + page
    if os.path.isfile(page_file):
        components = vbuild.render(page_file, render_paths)
        head = ""
        html = components.html
        style = components.style
        script = components.script.replace("Vue.component('" + page, 
                                           "Vue.component('" + page_name)

        # Infer layout
        layout_name = "default"
        layout_file = os.path.join('layouts', layout_name + '.vue')
        try:
            for item in re.findall('layout:?=? .*[\'"](.*)[\'"]', script):
                layout_candidate = os.path.join('layouts', item + '.vue')
                if os.path.isfile(layout_candidate):
                    layout_name = item
                    layout_file = layout_candidate
        finally:
            layout_target = 'layouts-' + layout_name
            if os.path.isfile(layout_file):
                layout = vbuild.render(layout_file)
                print(layout_name)

                # also, "default" is a reserved word in javascript
                layout_script = layout.script.replace("Vue.component('" + layout_name, 
                                                      "Vue.component('" + layout_target)\
                                             .replace("default = Vue.component(",
                                                      "layout_default = Vue.component(")\
                                             .replace("var default=(function()",
                                                      "var layout_default=(function()")

                script = script + "\n" + layout_script
                for item in re.findall('head:?=? .*[\'"](.*)[\'"]', script):
                    head += item

                html = html + "\n" + layout.html.replace("<qvp_page/>",  "<" + page_name + ">" \
                                                                      + "</" + page_name + ">")
                style = layout.style + "\n" + style
            else:
                html = html + "\n" + "<" + page_name + "></" + page_name + ">"

        return template.replace('<!-- html -->', html)\
                           .replace('<!-- head -->', head)\
                           .replace('/* script */', script)\
                           .replace('/* style */', style)\
                           .replace('<!-- layout -->', "<" + layout_target + ">" \
                                                    + "</" + layout_target + ">")
    return abort(404)



# Mimic Guy API

@app.websocket('/ws')
async def ws():
    await websocket.send('hello')

if __name__ == "__main__":
    app.run(host="0.0.0.0", port="9520")
