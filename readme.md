# qvp

*qvp* suele escribirse con minúsculas. En minúsculas se conserva la simetría. Del mismo modo el minimalismo conserva la belleza.

*qvp* es una herramienta minimalista para hacer aplicaciones y sitios web, apoyado en componentes Vue y (opcionalmente) Python.

# La Especificación *qvp* 

Este documento es una hoja de ruta para el desarrollo de *qvp*.

## La filosofía *qvp*

* Lo trivial debe lucir trivial, lo difícil alcanzable
* Ni magia ni redundancia, seamos claros, seamos concisos
* Minimalismo y flexibilidad

# Plan General

## Sitio estático (Objetivo #1)

* Generar rutas a partir de árbol de directorios (modelo: Nuxt.js)
* Compilar páginas a partir de componentes Vue.

## Backend WebSockets (Objetivo #2)

* Comunicación por WebSockets (invocar funciones, modelo: Guy)
* Sistema de eventos entre pares

## Backedn P2P (Objetivo #3)
* Sincronizar persistencia (con GunDB)
* Investigar HyperDB
